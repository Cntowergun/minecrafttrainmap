tellraw @p {"text":"===== Select Elevator Model =====","color":"gold"}
tellraw @p {"text":"[Model 1]","color":"dark_green","clickEvent":{"action":"run_command","value":"/execute @e[name=elevator,r=10,c=1] ~ ~ ~ function elevator_gshn28:change_model_1"}}
tellraw @p {"text":"[Model 2]","color":"dark_green","clickEvent":{"action":"run_command","value":"/execute @e[name=elevator,r=10,c=1] ~ ~ ~ function elevator_gshn28:change_model_2"}}
tellraw @p {"text":"[Model 3]","color":"dark_green","clickEvent":{"action":"run_command","value":"/execute @e[name=elevator,r=10,c=1] ~ ~ ~ function elevator_gshn28:change_model_3"}}
tellraw @p {"text":"[Model 4]","color":"dark_green","clickEvent":{"action":"run_command","value":"/execute @e[name=elevator,r=10,c=1] ~ ~ ~ function elevator_gshn28:change_model_4"}}
tellraw @p {"text":"[Model 5]","color":"dark_green","clickEvent":{"action":"run_command","value":"/execute @e[name=elevator,r=10,c=1] ~ ~ ~ function elevator_gshn28:change_model_5"}}
tellraw @p {"text":"[Modern]","color":"dark_green","clickEvent":{"action":"run_command","value":"/execute @e[name=elevator,r=10,c=1] ~ ~ ~ function elevator_gshn28:change_model_modern"}}
tellraw @p {"text":"[Classic]","color":"dark_green","clickEvent":{"action":"run_command","value":"/execute @e[name=elevator,r=10,c=1] ~ ~ ~ function elevator_gshn28:change_model_classic"}}
tellraw @p {"text":"[Panoramic]","color":"dark_green","clickEvent":{"action":"run_command","value":"/execute @e[name=elevator,r=10,c=1] ~ ~ ~ function elevator_gshn28:change_model_panoramic"}}
tellraw @p {"text":"==============================","color":"gold"}
